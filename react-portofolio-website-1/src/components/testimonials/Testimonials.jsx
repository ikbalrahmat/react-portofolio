/* eslint-disable jsx-a11y/img-redundant-alt */
import React from "react";
import "./testimonials.css";
import Avtr1 from "../../assets/avatar1.jpg";
import Avtr2 from "../../assets/avatar2.jpg";
// import Avtr3 from "../../assets/avatar1.jpg";
// import Avtr4 from "../../assets/avatar1.jpg";
// import Swiper core and required modules
import { Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

const Testimonials = () => {
  const data = [
    {
      avater: Avtr1,
      name: "Flutter Mobile App Development - Sanbercode",
      review:
        " I got a score of 85.00 with the title of Mastered graduation in this Bootcamp, get a line-up of project-based learning by creating mobile applications using flutter with the prototype from Figma that we have created and retrieve data using the API.",
      demo: "https://sanbercode.com/sertifikat/generate/8cff70b5-144c-4049-ba6f-f69a84a591d0",
    },
    {
      avater: Avtr2,
      name: "Organization Certificate - EBIO",
      review:
        " EEPIS Bidikimisi Organization (EBIO) - Surabaya, East Java Jan 2021 - Jan 2022 Staff Member ( KESMA ) Bidikmisi and Kipk Student Gathering Committee Chief Executive of Student Re-data Person in Charge of Re-data on Outstanding Students Career Development Program Committee (CDP) Village Care (PEDDES) - Surabaya, East Java Volunteer cares about the village (2021)",
      demo: "https://drive.google.com/file/d/1SD0JKgQaOgLGonqbjoEzOYKaTL7PtTjX/view?usp=share_link",
    },
    // {
    //   avater: Avtr3,
    //   name: "Flutter Mobile App Development - Sanbercode",
    //   review:
    //     " Using Digital Marketing is no longer an option, but a necessity in this digital era. I learned many things at Digital Marketing RevoU, including : 1. The difference between organic vs paid channels, acquisition vs retention channels. 2. Choose channels based on scalability, targeting, and customer acquisition cost (CAC). 3. Organic channels: SEO, Organic Social Media, Customer Relationship Management. 4. Paid channels: SEM, Social Media Marketing, Display Ads.",
    //   demo: "https://sanbercode.com/sertifikat/generate/8cff70b5-144c-4049-ba6f-f69a84a591d0",
    // },
    // {
    //   avater: Avtr4,
    //   name: "Feidlimid İlkay Abdullayev",
    //   review:
    //     " Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cupiditate numquam, tenetur iste autem voluptatem expedita minima inventore facilis libero sit, accusantium modi adipisci? Ipsam, eaque! Nemo rem earum expedita minima.",
    //   demo: "https://certificates.revou.co/abdul-aziz-certificate-completion-dmmc22.pdf",
    // },
  ];

  return (
    <section id="testimonials">
      <h5>What Certificate I Have</h5>
      <h2>My Certificate</h2>
      <Swiper
        className="container testimonials__container"
        modules={[Pagination]}
        spaceBetween={40}
        slidesPerView={1}
        pagination={{ clickable: true }}
      >
        {data.map(({ avater, name, review, demo }, index) => {
          return (
            <SwiperSlide key={index} className="testimonial">
              <div className="client__avatar">
                <img src={avater} alt="client image" />
              </div>
              <h5 className="client__name">{name}</h5>
              <small className="client__review">{review}</small>
              <a
                href={demo}
                className="btn btn-primary"
                target="_blank"
                rel="noreferrer"
              >
                View Credentials
              </a>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </section>
  );
};

export default Testimonials;
