import React from 'react';
import './about.css';
import MeAbout from '../../assets/me-about.jpeg';
import { FaAward } from "react-icons/fa";
import { FaCertificate } from "react-icons/fa";
import { VscFolderLibrary } from "react-icons/vsc";

const About = () => {
  return (
    <section id="about">
      <h5>Get To Know</h5>
      <h2>About me</h2>

      <div className="container about__container">
        <div className="about__me">
          <div className="about__me-image">
            <img src={MeAbout} alt="About Image" />
          </div>
        </div>
        <div className="about__content">
          <div className="about__cards">
            <article className="about__card">
              <FaAward className="about__icon" />
              <h5>Experience</h5>
              <small>2+ Software Engineer</small>
            </article>
            <article className="about__card">
              <FaCertificate className="about__icon" />
              <h5>Certificate</h5>
              <small>2+ Certificate</small>
            </article>
            <article className="about__card">
              <VscFolderLibrary className="about__icon" />
              <h5>Projects</h5>
              <small>5+ Completed</small>
            </article>
          </div>
          <p>
          I am a student of the Informatics Engineering Department, Surabaya State Electronics Polytechnic, 
          having an interest and looking for opportunities as a mobile developer and front-end developer 
          with a background in software engineering. I am experienced in Flutter, React-js, and Bootstrap. 
          Skilled at working in teams and as individuals. I'm excited to learn new technologies and new challenges.
          </p>
          <a href="#contact" className="btn btn-primary">
            Let's Talk With Me
          </a>
        </div>
      </div>
    </section>
  );
};

export default About;