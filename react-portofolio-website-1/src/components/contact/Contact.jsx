import React from 'react'
import './contact.css'
import {MdOutlineEmail} from 'react-icons/md'
import {RiMessengerLine} from 'react-icons/ri'
// import {FaTelegramPlane} from 'react-icons/fa'
// import { BsInstagram } from 'react-icons/bs'
// import {BsLinkedin} from 'react-icons/bs'
import {BsWhatsapp} from 'react-icons/bs'
import { useRef } from 'react';
import emailjs from 'emailjs-com';

const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_r0q6vdl', 'template_p0x83oz', form.current, 'S6WN5uFTyEw8DMrKr')
      
    e.target.reset()
  };  
  return (
    <section id='contact'>
      <h5>Get in Touch</h5>
      <h2>Contact Me</h2>
      <div className="container contact__container">
        <div className="contact__options">
          <article className='contact__option'>
            <MdOutlineEmail className='contact__option-icon'/>
            <h4>Email</h4>
            <h6>ikbalrahmattaupik62@gmail.com</h6>
            <a href="mailto:ikbalrahmattaupik62@gmail.com" target="_blank" rel="noreferrer">Send a message</a>
          </article>
          <article className='contact__option'>
            <RiMessengerLine  className='contact__option-icon'/>
            <h4>Messenger</h4>
            <h5>facebook.com</h5>
            <a href="https://www.facebook.com/watch/?v=474604240584615" target="_blank">Send a message</a>
          </article>
          <article className='contact__option'>
            <BsWhatsapp  className='contact__option-icon'/>
            <h4>WhatsApp</h4>
            <h5>+6285624862682</h5>
            <a href="https://wa.me/6285624862682" target="_blank" rel="noreferrer">Send a message</a>
          </article>
        {/* </div>
        <div className="contact__options">
          <article className='contact__option'>
            <BsLinkedin className='contact__option-icon'/>
            <h4>Linkedin</h4>
            <h6>abdul-aaziz</h6>
            <a href="https://www.linkedin.com/in/abdul-aaziz/" target="_blank" rel="noreferrer">Send a message</a>
          </article> */}
          {/* <article className='contact__option'>
            <RiMessengerLine  className='contact__option-icon'/>
            <h4>Messenger</h4>
            <h5>facebook.com</h5>
            <a href="https://facebook.com" target="_blank">Send a message</a>
          </article> */}
          {/* <article className='contact__option'>
            <BsInstagram  className='contact__option-icon'/>
            <h4>Instagram</h4>
            <h5>hi_aazzizz</h5>
            <a href="https://www.instagram.com/hi_aazzizz/" target="_blank" rel="noreferrer">Send a message</a>
          </article> */}
        </div>
        {/* END OF CONTACT OPTION */}
        <form ref={form} onSubmit={sendEmail}>
          <input type="text" name='name' placeholder='Your Full Name' required />
          <input type="text" name='email' placeholder='Your Email' required />
          <textarea name="message" rows="7" placeholder='Your message' required></textarea>
          <button type='submit' className='btn btn-primary'>Send Message</button>
        </form>
      </div>
    </section>
  )
}

export default Contact