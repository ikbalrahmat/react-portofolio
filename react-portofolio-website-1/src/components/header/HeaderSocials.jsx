import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {FaGithub} from 'react-icons/fa'
import {FiGitlab} from 'react-icons/fi'

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href="https://www.linkedin.com/in/ikbal-rahmat-taupik-2980441b9/" target="_blank"><BsLinkedin/></a>
        <a href="https://gitlab.com/ikbalrahmat" target="_blank"><FiGitlab/></a>
        <a href="https://github.com/ikbalrahmat" target="_blank"><FaGithub/></a>
    </div>
  )
}

export default HeaderSocials