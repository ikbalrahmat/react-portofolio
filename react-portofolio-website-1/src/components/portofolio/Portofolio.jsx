import React from 'react'
import './portofolio.css'
import Img1 from '../../assets/portofolio1.png'
import Img2 from '../../assets/portofolio2.png'
import Img3 from '../../assets/portofolio3.png'
// import Img4 from '../../assets/portofolio4.jpg'
// import Img5 from '../../assets/portofolio5.png'
// import Img6 from '../../assets/portofolio6.jpg'

const Portofolio = () => {

  const data=[
    {
      id:1,
      image:Img1,
      title:"HairM Website",
      review: " HairM is an application that provides references to the most popular haircut models and also recommends them for you so that they can help determine the best haircut model that suits you!",
      // review: " 1. Collaboration with programmers and stakeholders to meet project requirements, goals, and desired functionality website. 2. Manage website development from designing, publishing, and optimizing. 3. Development website from backend Restful API Laravel to frontend using React-JS. 4. Carry out website development to make the feature of changing clothes by using the index efficiently.",
      gitlab:"https://gitlab.com/habil0105/hairm",
      demo:"https://hairm.studio/",
    },
    {
      id:2,
      image:Img2,
      title:"HairM Mobile App",
      review: " HairM is an application that provides references to the most popular haircut models and also recommends them for you so that they can help determine the best haircut model that suits you!",
      // review: " 1. Build a mobile application with standard object oriented programming and clean code for reusable programs 2. Develop mobile applications according to market needs by collaborating with customers according to the features needed and successfully proven by user satisfaction getting 5 stars in the play store 3. Implement the latest technology in mobile development to maintain high performance.",
      gitlab:"https://gitlab.com/habil0105/cukuran",
      demo:"https://play.google.com/store/apps/details?id=com.cukuran.hairm&gl=US",
    },
    {
      id:3,
      image:Img3,
      title:"Chat App",
      review: "1.	Create ERDs, Migrations, Model + Eloquent, Controllers. 2.	Laravel Auth + Middleware for Login and Register 3.	CRUD (Create Read Update Delete) features (product CRUD, category CRUD, basket CRUD, Profile CRUD.). 4.	Eloquent Relationships (One To One, One To Many, Many To Many). 5.	Libraries/Packages outside of Laravel (Sweet alert, DomPDF, and TinyMCE). 6. Deployment Laravel website version 9 in hostingan.id",
      // review: "1.	Create ERDs, Migrations, Model + Eloquent, Controllers. 2.	Laravel Auth + Middleware for Login and Register 3.	CRUD (Create Read Update Delete) features (product CRUD, category CRUD, basket CRUD, Profile CRUD.). 4.	Eloquent Relationships (One To One, One To Many, Many To Many). 5.	Libraries/Packages outside of Laravel (Sweet alert, DomPDF, and TinyMCE). 6. Deployment Laravel website version 9 in hostingan.id",
      gitlab:"https://gitlab.com/ikbalrahmat/chatappflutter",
      demo:"https://gitlab.com/ikbalrahmat/chatappflutter/"
    },
    // {
    //   id:4,
    //   image:Img4,
    //   title:"Furniture Website",
    //   review: "1. Manage website development from designing, publishing, and optimizing. 2. Manage website development from designing, publishing, and optimizing. 3. Build a furniture website using React JS. 4. Build a website that is responsive to all existing devices",
    //   github:"https://github.com/aazzizzz1/skawanfrontenddev-1-abdulaziz",
    //   demo:"https://skawanfrontenddev-1-abdulaziz.netlify.app/",
    // },
    // {
    //   id:5,
    //   image:Img5,
    //   title:"Find Jobs Mobile App",
    //   review: "1. Made my own app design using Figma and implemented mobile app development using Flutter. 2. Authentication using firebase via user email. 3. Deployment of mobile apps to Google Drive. 4. Development website from API public to Flutter code",
    //   github:"https://gitlab.com/azizvbi33/bootcampfluttersanber/-/tree/main/FinalProject",
    //   demo:"https://drive.google.com/file/d/1gY_wmx0TZJgy5Ws60C6hXM-BbVnofxjj/view?usp=share_link"
    // },
    // {
    //   id:6,
    //   image:Img6,
    //   title:"This is a portofolio item title",
    //   github:"https://github.com",
    //   demo:"https://dribbble.com"
    // },
  ]

  return (
    <section id="portofolio">
      <h5>My Recent Work</h5>
      <h2>Portofolio</h2>
      <div className="container portofolio__container">
        {data.map(({id,image,title,review,gitlab,demo})=>{
          return(
              <article className="portofolio__item">
              <div className="portofolio__item-image">
                <img src={image} alt={title} />
              </div>
              <h3>{title}</h3>
              <small className="myreview">{review}</small>
              <div className="portofolio__item-cta">
                <a href={gitlab} className="btn" target="_blank" rel="noreferrer">
                  Gitlab
                </a>
                <a
                  href={demo}
                  className="btn btn-primary"
                  target="_blank" rel="noreferrer"
                >
                  Live Demo
                </a>
              </div>
            </article>
            )
        })}
      </div>
    </section>
  );
};

export default Portofolio