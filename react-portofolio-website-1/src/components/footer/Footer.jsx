/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import './footer.css'
import {FiInstagram} from 'react-icons/fi'
import {BsLinkedin} from 'react-icons/bs'
import {FiGitlab} from 'react-icons/fi'
import{FaGithub} from 'react-icons/fa'
// import {FaFacebookF} from 'react-icons/fa'
// import {IoLogoTwitter} from 'react-icons/io'

const Footer = () => {
  return (
    <footer>
      <a href="#" className="footer__logo">Thank You for Visiting</a>
      <ul className="permalinks">
        <li><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#experience">Skills</a></li>
        {/* <li><a href="#services">Services</a></li> */}
        <li><a href="#portofolio">Portofolio</a></li>
        <li><a href="#testimonials">Certificate</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>

      <div className="footer__socials">
       <a href="https://www.linkedin.com/in/ikbal-rahmat-taupik-2980441b9/"><BsLinkedin/></a>
       <a href="https://www.instagram.com/ikbalrahmat_/"><FiInstagram/></a>
       <a href="https://gitlab.com/ikbalrahmat"><FiGitlab/></a>
       <a href="https://github.com/ikbalrahmat"><FaGithub/></a>
      </div>

      <div className="footer__copyright">
        <small>&copy; Ikbal Rahmat Taupik. All rights reserved.</small>
      </div>
    </footer>
  )
}

export default Footer